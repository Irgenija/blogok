from django.urls import path

from blogok_api.apps.users.views import UserDetail, UserListCreateView

urlpatterns = [
    path('', UserListCreateView.as_view(), name='users'),
    path('<int:pk>/', UserDetail.as_view(), name='user_details'),
]
