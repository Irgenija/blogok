from rest_framework import generics, permissions
from rest_framework.permissions import IsAuthenticated

from blogok_api.apps.core.views import AllowedMethodsMixin
from blogok_api.apps.users.models import User
from blogok_api.apps.users.serializers import UserRegisterSerializer, \
    UserSerializer


class UserListCreateView(generics.ListCreateAPIView):
    """ List of users and create user. """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.request.method == "POST":
            return [permissions.AllowAny()]

        return [permissions.AllowAny()]

    def get_serializer_class(self):
        if self.request.method == "POST":
            return UserRegisterSerializer

        return self.serializer_class


class UserDetail(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete user. """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
