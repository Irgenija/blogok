from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'blogok_api.apps.users'
