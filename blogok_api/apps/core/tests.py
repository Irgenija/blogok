from rest_framework.reverse import reverse


class BaseUserTest:
    """ Login User for test case. """

    fixtures = ("users",)

    def setUp(self):
        super().setUp()

        self.token = self.get_token(
            login='Username_1',
            password='123',
        )

    def get_token(self, login: str, password: str) -> str:
        """ Login user and combine Auth header with user Token. """

        data = {
            'username': login,
            'password': password,
        }

        response = self.client.post(reverse('token_obtain_pair'), data)

        return response.json()['access']
