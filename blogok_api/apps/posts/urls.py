from django.urls import path

from blogok_api.apps.posts.views import PostDetail, PostListCreateView, \
    ListOfUsersPosts

urlpatterns = [
    path('', PostListCreateView.as_view(), name='posts'),
    path('<int:pk>/', PostDetail.as_view(), name='post_details'),
    path(
        'author/<int:author_id>/',
        ListOfUsersPosts.as_view(),
        name='user_posts',
    ),
]
