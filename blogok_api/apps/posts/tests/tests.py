from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from blogok_api.apps.core.tests import BaseUserTest
from blogok_api.apps.posts.models import Post


class APIPostsTest(BaseUserTest, APITestCase):
    """ Users tests. """

    fixtures = ("users", "posts")

    def test_get_posts_list(self):
        response = self.client.get(
            reverse('posts'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_posts_list(self):
        Post.objects.all().delete()
        response = self.client.get(
            reverse('posts'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)

    def test_get_post_details(self):
        post = Post.objects.get(pk=1)
        post.tags.add("test_tag_1")
        post.tags.add("test_tag_2")
        post.save()

        response = self.client.get(
            reverse('post_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': 1,
            'author': 1,
            'title': 'Test post 1',
            'text': 'Text of test post 1',
            'created_date': '2021-01-17T23:31:00.389000+02:00',
            'published_date': '2021-01-17T23:31:00.389000+02:00',
            'tags': ["test_tag_1", "test_tag_2"],
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_get_not_existed_post_details(self):
        response = self.client.get(
            reverse('post_details', args=[4]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_create_post(self):
        data = {
            'author': 1,
            'title': 'Test post 3',
            'text': 'Text of test post 3',
            'created_date': '2021-01-17T23:31:00.389000+02:00',
            'published_date': '2021-01-17T23:31:00.389000+02:00',
            'tags': "test_tag_3,test_tag_4",
        }

        response = self.client.post(
            reverse('posts'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format="json",
        )
        expected = {
            'id': 3,
            'author': 1,
            'title': 'Test post 3',
            'text': 'Text of test post 3',
            'created_date': '2021-01-17T23:31:00.389000+02:00',
            'published_date': '2021-01-17T23:31:00.389000+02:00',
            'tags': ['test_tag_3', 'test_tag_4'],
        }

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(expected, response.json())

    def test_create_post_without_required(self):
        data = {
            'author': '',
            'title': 'Test post 3',
            'text': 'Text of test post 3',
            'created_date': '2021-01-17T23:31:00.389000+02:00',
            'published_date': '2021-01-17T23:31:00.389000+02:00',
            'tags': "",
        }

        response = self.client.post(
            reverse('posts'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format="json",
        )
        expected = {'author': ['This field may not be null.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_post(self):
        post = Post.objects.get(pk=1)
        post.tags.add("test_tag_1")
        post.tags.add("test_tag_2")
        post.save()

        data = {
            'title': 'Test post 3',
        }

        response = self.client.patch(
            reverse('post_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': 1,
            'author': 1,
            'title': 'Test post 3',
            'text': 'Text of test post 1',
            'created_date': '2021-01-17T23:31:00.389000+02:00',
            'published_date': '2021-01-17T23:31:00.389000+02:00',
            'tags': ["test_tag_1", "test_tag_2"],
        }

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_update_post_validation(self):
        post = Post.objects.get(pk=1)
        post.tags.add("test_tag_1")
        post.tags.add("test_tag_2")
        post.save()

        data = {
            'author': '',
        }

        response = self.client.patch(
            reverse('post_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'author': ['This field may not be null.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_post_not_existed(self):
        data = {
            'title': "Test 1 1",
        }

        response = self.client.patch(
            reverse('post_details', args=[5]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_delete_post(self):
        response = self.client.delete(
            reverse('post_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_post_not_existed(self):
        response = self.client.delete(
            reverse('post_details', args=[5]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauthorized_get_users_posts_list(self):
        response = self.client.get(reverse('user_posts', args=[1]))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_users_posts_list(self):
        response = self.client.get(
            reverse('user_posts', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_users_posts_list(self):
        Post.objects.all().delete()
        response = self.client.get(
            reverse('user_posts', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)
