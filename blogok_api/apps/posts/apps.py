from django.apps import AppConfig


class PostsConfig(AppConfig):
    name = 'blogok_api.apps.posts'
