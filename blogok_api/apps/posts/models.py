from django.db import models
from django.utils import timezone
from taggit.managers import TaggableManager

from blogok_api.apps.users.models import User


class Post(models.Model):
    """ Post model. """

    author = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    title = models.CharField(max_length=200, help_text="Enter title of post")
    text = models.TextField(
        max_length=1000,
        null=True,
        help_text="Enter the text",
    )
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    tags = TaggableManager()

    def get_tags(self):
        """ names() is a django-taggit method, returning a ValuesListQuerySet
        (basically just an iterable) containing the name of each tag as a
        string. """
        return self.tags.names()

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
