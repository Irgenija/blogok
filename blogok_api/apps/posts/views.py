from rest_framework import generics, filters
from rest_framework.permissions import IsAuthenticated

from blogok_api.apps.core.views import AllowedMethodsMixin
from blogok_api.apps.posts.models import Post
from blogok_api.apps.posts.serializers import PostSerializer, \
    PostDetailSerializer, ListOfUsersPostsSerializer


class PostListCreateView(generics.ListCreateAPIView):
    """ List of posts and create post. """

    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title', 'text')

    def get_serializer_class(self):
        if self.request.method == "POST":
            return PostDetailSerializer

        return self.serializer_class


class PostDetail(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete post. """

    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = [IsAuthenticated]


class ListOfUsersPosts(generics.ListAPIView):
    """ List of users posts. """

    queryset = Post.objects.all()
    serializer_class = ListOfUsersPostsSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(author_id=self.kwargs['author_id']).all()
