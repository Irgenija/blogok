from rest_framework import serializers

from blogok_api.apps.posts.models import Post


class PostSerializer(serializers.ModelSerializer):
    """ Post information. """

    class Meta:
        model = Post
        fields = (
            'id',
            'author',
            'title',
            'published_date',
        )


class TagsField(serializers.Field):
    """ custom field to serialize/deserialize TaggableManager instances.
    """
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class PostDetailSerializer(serializers.ModelSerializer):
    """ Post detail information. """

    tags = TagsField(source="get_tags")

    class Meta:
        model = Post
        fields = (
            'id',
            'author',
            'title',
            'text',
            'created_date',
            'published_date',
            'tags',
        )

    def create(self, validated_data):
        tags = validated_data.pop("get_tags")
        post = Post.objects.create(**validated_data)
        if tags:
            tags = tags.split(",")
            post.tags.add(*tags)
        return post


class ListOfUsersPostsSerializer(serializers.ModelSerializer):
    """ List of users posts serializer. """

    class Meta:
        model = Post
        fields = (
            'id',
            'title',
            'published_date',
        )
